# Change logs

## v1.2

Release date 27th of January, 2019

 - Support Markdown v2.x and v3.x
 
## v1.0

Release date 21th of November, 2018

 - Initial Release
